variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "selgharabawy@outlook.com"
}

variable "ami" {
  default = "amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}